---
title: "Python Typing Koans"
date: 2021-06-27T16:00:00+05:30
draft: false
tags: ["python", "Django", "gradual-typing", "type-hints"]
---

Python 3 introduced type annotation syntax. [PEP 484](https://www.python.org/dev/peps/pep-0484/) introduced a provisional module to provide these standard definitions and tools, along with some conventions for situations where annotations are not available.

Python is a dynamic language and follows gradual typing. When a static type checker runs a python code, the type checker considers code without type hints as `Any`.

```
def print_name(name):
       print(name)

planet: str = "earth"
```

In the above example, the `name` argument type hint will be `Any` since the type hint is missing while the type hint for the `planet` variable is a string.

Gradual typing is still an emerging topic in Python, and there is a gap in resources to educate the Python developers about the utility and Python typing concepts.

On the surface, it looks easy to annotate the code. But the dynamic nature makes a certain part of the code harder to annotate. I have been using type-hints over the past three years and find it hard sometimes. A lot of new developers also face the same problem.

## Koans

To make the learning easier, simpler, I have created a [GitHub repository](https://github.com/kracekumar/python-typing-koans).

The repository contains the standalone python programs. The programs contain partial or no type hints. By adding new type hints and fixing the existing type hints, the learner will understand how type-checkers evaluate the types and what's a difference in types at run-time.

Here is a simple demo to use of the command line.

[![asciicast](https://asciinema.org/a/419119.svg)](https://asciinema.org/a/419119)

## Steps

1. Clone the repository. `git clone git@github.com:kracekumar/python-typing-koans.git`
2. Install all the dependencies(advised to use Python Poetry, virtual env should also work.). `poetry install`. It requires Python 3.9.4
3. List all the koans using the command line program. `poetry run python cli.py list`
4. Pick up a file to learn.
5. Run the file with the command line program. `poetry run python cli.py one koans/py/100-easy-variable-wrong-type.py`
6. Repeat the process till there are no type errors.

*One central missing part is how the learner will know to fix the type errors?*

The comments in the files carry the links to relevant concepts, which aids the learner in understanding the ideas to use.

Screenshots of a few koans

![Python Callables](/images/python_typing_koans/python_callables.png)

![Easy Protocol](/images/python_typing_koans/protocol.png)

![Medium Protocol](/images/python_typing_koans/python_protocol_medium.png)


## Topics

Python topics covered are
- Primitive Types
- dictionaries - dict/typedict
- Callables
- Design pattern - factory pattern, the builder pattern
- Decorators
- Type Alias
- Protocol, classes, objects

20 Python programs(koans) help the learner to understand gradual typing. The filenames indicate the learning level like `easy, medium, and hard.`

The repository also contains Django and Django Rest Framework examples.

The Django koans teach the annotating `views, models, model methods, queryset methods like filter, all, annotate, aggregate, Q object` etc..

![Django Annotate](/images/python_typing_koans/django_annotate_aggregate.png)

![Django Queryset](/images/python_typing_koans/django_queryset.png)


The DRF koans teach how to annotate  `DRF serializers and DRF Views`.


![DRF View](/images/python_typing_koans/drf_view.png)

![DRF Serializer](/images/python_typing_koans/drf_serializer.png)


If you face any issues while solving the koans, please open an issue in the Github repository; I'd happy to answer and explain the relevant concepts.

## Links

- PEP 484 - https://www.python.org/dev/peps/pep-0484/
- Github Repository - https://github.com/kracekumar/python-typing-koans
- Python typing documentation - https://docs.python.org/3/library/typing.html
