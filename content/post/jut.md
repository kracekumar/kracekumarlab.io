---
title: "jut - render jupyter notebook in the terminal"
date: 2021-03-19T01:00:00+05:30
draft: false
tags: ["python", "CLI", "rich", "jupyter", "pet-project"]
---


The Jupyter Notebook is an open-source web application that allows you to create and share documents that contain live code, equations, visualizations and narrative text. Uses include: data cleaning and transformation, numerical simulation, statistical modeling, data visualization, machine learning, and much more. The definition copied from the [official website](https://jupyter.org/).

It's becoming common to use Jupyter notebook to [write books](https://github.com/fastai/fastbook), do data analysis, reproducible experiments, etc... The file produced out of notebook follows [JSON Schema](https://github.com/jupyter/nbformat/blob/master/nbformat/v4/nbformat.v4.schema.json). Yet to view the file, the user needs to use [web-application](https://github.com/jupyter/nbviewer) or local notebook instance or browser instance.

Here is a 20 lines output of a notebook.

```shell
$cat 02.02-The-Basics-Of-NumPy-Arrays.ipynb | head -20
{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<!--BOOK_INFORMATION-->\n",
    "<img align=\"left\" style=\"padding-right:10px;\" src=\"figures/PDSH-cover-small.png\">\n",
    "\n",
    "*This notebook contains an excerpt from the [Python Data Science Handbook](http://shop.oreilly.com/product/0636920034919.do) by Jake VanderPlas; the content is available [on GitHub](https://github.com/jakevdp/PythonDataScienceHandbook).*\n",
    "\n",
    "*The text is released under the [CC-BY-NC-ND license](https://creativecommons.org/licenses/by-nc-nd/3.0/us/legalcode), and code is released under the [MIT license](https://opensource.org/licenses/MIT). If you find this content useful, please consider supporting the work by [buying the book](http://shop.oreilly.com/product/0636920034919.do)!*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<!--NAVIGATION-->\n",
    "< [Understanding Data Types in Python](02.01-Understanding-Data-Types.ipynb) | [Contents](Index.ipynb) | [Computation on NumPy Arrays: Universal Functions](02.03-Computation-on-arrays-ufuncs.ipynb) >\n",
```

It's hard to follow the **content along with the schema**. When the code is checked into the repository, it's hard to view the content from the **command line**.

Over the weekend, I wrote a small command-line tool to render the notebook over the terminal with help of [fantastic python library, rich.](https://github.com/willmcgugan/rich)

### Tool

[jut](https://github.com/kracekumar/jut) is a command line tool to display jupyter notebook in the terminal. The tool takes in a jupyter notebook file or URL (Raw Github or any ipynb file link) and renders each cell content in the terminal. You can install the package by running pip command `pip install jut`.

Here is a quick **asciicinema demo**

[![asciicast](https://asciinema.org/a/400349.svg)](https://asciinema.org/a/400349)

### Usage

### Display first five cells

Example: `jut --input-file foo.ipynb --head 5`

![https://raw.githubusercontent.com/kracekumar/jut/main/images/jut-head.png](https://raw.githubusercontent.com/kracekumar/jut/main/images/jut-head.png)

### Display last five cells

Example: `jut --input-file foo.ipynb --tail 5`

![https://raw.githubusercontent.com/kracekumar/jut/main/images/jut-tail.png](https://raw.githubusercontent.com/kracekumar/jut/main/images/jut-tail.png)

### Download the file and display first five cells

Example: `jut --url https://raw.githubusercontent.com/fastai/fastbook/master/06_multicat.ipynb --head 5`

![https://raw.githubusercontent.com/kracekumar/jut/main/images/jut-download.png](https://raw.githubusercontent.com/kracekumar/jut/main/images/jut-download.png)

### Limitation

The tool only display **text content like markdown, python code, python output, raw text, JSON** and ignores any multi-media contents like images, PDF documents.

I hope the tool is a useful utility for folks working and handling with the Jupyter notebook. The code is available in [GitHub](https://github.com/kracekumar/jut), feel free to file bug report and file feature requests.


### References

- Jupyter Project - https://jupyter.org/
- Github Page - https://github.com/kracekumar/jut
- Python Rich Library - https://github.com/willmcgugan/rich
