---
title: "ChatGPT Shambles for Gary Marcus Prompt"
date: 2025-02-08T22:14:34Z
draft: false
tags:
- chatgpt
- llm
- AI
---

Gary Marcus recently wrote an article titled [ChatGPT in Shambles](https://garymarcus.substack.com/p/chatgpt-in-shambles).
The prompt instructed chatgpt to produce a tabular table of median house hold income across [U.S. states](https://chatgpt.com/share/67a20c79-bfa4-8001-8cdf-4ee60d42df5f).

```
Make a table of every state in U.S., including population, area, median house hold income, sorted in order of median household income.
```


![chatgpt-original-output](/images/chatgpt-shambles/chatgpt_original.png)
The output contained only twenty states and interrupted. The final row contained only name of the state.

### ChatGPT - My Attempt
The [same prompt](https://chatgpt.com/share/67a7d85c-6af0-8001-bb5e-d01d816b59f7) returned all the states and income, when I tried and logged into the ChatGPT. I skipped verifying the data quality and checked only the structure.

![chatgpt-1](/images/chatgpt-shambles/chatgpt_1.png)
![chatgpt-2](/images/chatgpt-shambles/chatgpt_2.png)
![chatgpt-3](/images/chatgpt-shambles/chatgpt_3.png)

My guess is fine-tuned(don't think so in the short interval) or non-deterministic output based on logged in user vs anonymnous ask.

I tried the same prompt in other models

### Claude

Produced well structured output with an extra summary and further asking for more task.

![claude-output](/images/chatgpt-shambles/claude.png)

### Deepseek

![deepseek-output](/images/chatgpt-shambles/deepseek.png)
Similar to Claude's output Deepseek did produce all states including a summary.


[Gemini 2.0 Flash](https://g.co/gemini/share/ea864e8105c1)

![gemini-flash-pro-2.0-output](/images/chatgpt-shambles/gemini.png)
By the far the Gemini output is well-structured with rank column, option to export the results to google sheets and summary at the end.

### Le Chat
![lechat](/images/chatgpt-shambles/le_chat.png) [Le Chat](https://chat.mistral.ai/chat) produced all the fifty states with the sources.

In the overall exercise it's clear to see small variation across models and clearly other models produce better output compared to ChatGPT.
It's confusing to see different behaviour from ChatGPT.
