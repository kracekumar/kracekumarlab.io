---
title: "TIL - A new site"
date: 2021-08-28T23:15:00+05:30
draft: false
tags: ["TIL"]
---

Quite often, as a programmer, I learn something new. Some are utilitarian; some are philosophical; some are opinions in programming. I want to document these learnings for later use and also to remember. So I'm starting a new site, [til.kracekumar.com](https://til.kracekumar.com), to demonstrate this learning. So far, there are [six posts](til.kracekumar.com).

The inspiration comes from [Simon Willson's TIL website](https://til.simonwillison.net). 

![](/images/til.png)

## Why the new site?

Two reasons

1. I'm planning to write often; sometimes, the post will fit in just two tweets.
2. I don't want the existing followers of the site to see a lot of small and new content.

In case you'd to follow the learnings and tips, [you can subscribe to RSS feed](https://til.kracekumar.com/index.xml).

## Website Setup

- The site uses a static site generator, [Hugo](https://gohugo.io/).
- The repo resides in [Gitlab](https://gitlab.com/kracekumar/til).
- The analytics and visitor tracking using [plausible.io](https://plausible.io/).
- The custom website dark theme, [photophobia](https://github.com/kracekumar/photophobia) forked from [setsevireon/photophobia](https://github.com/setsevireon/photophobia).

## Links

- TIL: https://til.kracekumar.com
- Simon Willson's TIL: https://til.simonwillison.net
- RSS feed: https://til.kracekumar.com/index.xml
- Hugo: https://gohugo.io/
- Gitlab Repo: https://gitlab.com/kracekumar/til
- Plausible: https://plausible.io/
- Photophobia theme: https://github.com/setsevireon/photophobia
