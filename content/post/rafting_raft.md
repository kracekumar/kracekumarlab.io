---
title: "Rafting Raft Workshop"
date: 2022-05-08T14:30:27+05:30
draft: false
Tags: ["Raft", "workshop"]
---

Last week, May 2-6, 2022, I attended [Rafting Raft](http://www.dabeaz.com/raft.html) workshop by [David Beazley](http://www.dabeaz.com/index.html).
The workshop focussed on building a raft implementation and it was intense and exhausting.

A few folks had asked me about the workshop and how it works. The post focuses on what happens before and during the workshop,
so future attendees can decide.

# Day -43

Someday when you're contemplating attending the workshop, you register on [the website](http://www.dabeaz.com/raft.html).
You get a confirmation email from David Beazley to confirm your availability to attend the workshop.

Email looks like

> Thanks for your interest in the upcoming Raft course.   I can confirm that space is available.  I simply need you to confirm your email by replying to this message.  Once I hear back, I will provide more details about logistics and other details.   Hopefully, I'll see you in the course!

# Day -30

You will receive a stripe link to pay the workshop fee. The workshop cost was $1250. Is it worth paying $1250 for 5 days workshop? It's subjective!

# Day -15

You will receive an email asking for a Github username for [collaboration](https://github.com/dabeaz-course) to a private repo.

# Day -7

Later you will be added to a Github repo with instructions to follow and preparation materials. The live discussion during the session happens in gitter channel.

# Day -1

Reminder about the workshop and materials to read up and programming warmup tasks - socket programming, concurrency primitives.
You introduce yourself in a Github thread.

# Day 0, -3:00:00

Reminder about the workshop.

# Day 0

The workshop started on India time, 8:00 PM, and went to till 4:00 AM in Zoom.

The first day was divided into a presentation session and an implementation session.
In the presentation session, Beazley walked us through the housekeeping tasks, - introduction, the structure of the workshop, and warm-up tasks.

In the implementation session, David started sharing his setup and started coding the tasks.
The first day was warmup tasks on `socket programming`, `concurrency primitives`, and `implementing a simple Key-Value server`
that will be used as an application for forthcoming raft implementation.

Another implementation session was focused on building a traffic system with two traffic lights and some constraints.

The first day was filled with the necessary concepts for next four days.

All the presentation sessions were recorded and uploaded to Vimeo by the end of the day.

These presentation sessions were lively and hands-on, the participants ask questions during the session.

The format

- Presentation/Discussion - 1 hour
- Project implementation - 2 hours
- Presentation/Discussion - 1 hour
- Break - 1 hour
- Implementation - 2 hour
- Discussion - 1 hour


At the end of the day, it was clear, what to expect and how you will go about implementing raft.

The entire implementation was split into multiple projects

- Project 1 - Key-Value Server
- Project 2 - Event Driven Programming
- Project 3 - Starting Out/Planning
- Project 4 - The Log
- Project 5 - Log Replication
- Project 6 - Consensus
- Project 7 - Leader Election
- Project 8 - Testing
- Project 9 - Simulation
- Project 10 - The System
- Project 11 - The Rest of Raft


# Day 1

The day started with the discussion on how to layout the project - different message types, components, timers, elections, log replications etc. How should we approach implementing the algorithm?
All the participants started building an outline of the project - `handlers, messages, enums, classes, etc...`

After the break, David discussed the topic of `log and log replication.`
He gave the rationale why this is core and the first and most important piece to implement.

Everyone pushed their code to a separate branch in a shared Github repository including David.
So it was easy to cross verify the implementation and look it up.

There was discussion on how to test the components, especially unit tests.

Everyone used Python and but you're free to use any language.

# Day 2

We're halfway through the workshop. At this point, more or less project structure, the core modules, and the style of programming are clear.
Now the use of event-driven programming and the simplicity of using queues to communicate is clear.

The main discussion of the day was `consensus` while applying the requests. Without consensus from the raft, the application cannot commit the changes. The implementation was focused on getting consensus working.

# Day 3

David started the day with TLA+ and he implemented a simple TLA+ for a traffic system that was implemented earlier.
He shared his experience with using TLA+ for distributed systems.

While looking at the [Raft TLA+](https://github.com/ongardie/raft.tla/blob/master/raft.tla), we noticed extra fields in the
RPC messages.

Then we continued implementation of the project on pending tasks in log replication and consensus.

After the break, David presented the leader election along with various cases and edge cases that aren't clear from the paper.
Then we continued to implement the leader election for the rest day.

# Day 4

On the last day, David discussed how to design the application to communicate with the raft server.
For all practical purposes, the raft was implemented like a module inside the Key-Value server.

Everyone agreed the code looked like a big ball of mud.
The complexity of designing the distributed system in 4 days with
perfect separation of concern and testable code is hard. The question of testing was the main point during every component implementation. Especially, how do we do integration tests?

We continued to implement the rest of the pending pieces in the project.
I was tired by the middle of the day and slept off :P

# Conclusion

- It was neatly organized and focused on implementing the raft **correctly**.
I stress correctly because it is easy to overlook certain portions and paper ends up implementing something close enough. How do you verify the correctness of implementation?

- David has done the workshop more than 10 times and focussing on one topic a day helped to
understand and implement.

- I haven't completed the entire raft implementation. Need to complete leader election edge cases and backtracking out-of-sync followers.

- The workshop was attended by 13 folks - the participants were from India, the UK, France, and the USA.

- The workshop format may change in the future, it's better to email all the doubts to David Beazley.

- It's a fun and intense workshop. Give it a try!

# Raft Links

- [Raft web page](https://raft.github.io/)
- [Raft TLA+](https://github.com/ongardie/raft.tla/blob/master/raft.tla)
- [Implementing Raft by Eli Bendersky](https://eli.thegreenplace.net/2020/implementing-raft-part-0-introduction/)
- [Chelsa Troy's Raft Tutorial](https://chelseatroy.com/tag/raft/)
- [Indradhanush Notes on Raft](https://indradhanush.github.io/blog/notes-on-raft/)
