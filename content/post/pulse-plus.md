---
title: "Pulse Plus"
date: 2021-09-06T01:37:00+05:30
draft: false
Tags: ["CLI", "pet-project", "PhonePe"]
---

[PhonePe](https://twitter.com/PhonePe_/status/1434054060148084736 )  recently released Pulse repo from their [payment data](https://github.com/PhonePe/pulse). It was hard to get an overview of the data without doing some data transformation.

The data is eight levels deep, nested, and multiple files for similar purpose data. Hard to do any command-line aggregate queries for data exploration. 

![](/images/pulse/pulse_data_tree.png)

It's hard to do any analysis with 2000+ files. So I created an [SQLite](https://github.com/kracekumar/pulse-plus/blob/main/data/v1/pulse.db ) database of the [data using python sqlite-utils](). 

The SQLite database aggregated data and top data in 5 tables - aggregated_user, aggregated_user_device, aggregated_transaction, top_user, top_transaction. Link to the schema - https://github.com/kracekumar/pulse-plus#all-tables-schema.

`python pulse/cli.py ../pulse/data --output pulse.db` creates the SQLite file from the pulse repo data.

The same five tables are available as five CSV files in [data/v1/ sub-directory of the repo](https://github.com/kracekumar/pulse-plus/tree/main/data/v1). [All aggregated transaction CSV file](https://github.com/kracekumar/pulse-plus/blob/main/data/v1/aggregated_transaction.csv).

![](/images/pulse/csv_file_list.png)

The data is flat(now) in CSV files and SQLite files, easy to explore in notebooks, metabase, or any data exploration tools. If you're comfortable with sql, analyze it using the [datasette tool](https://datasette.io/). 

![](/images/pulse/datasette_example.png)

## High-level data quality observations. 

- There is no currency unit in any of the datasets for the amount field. 🤦Is the transaction represented in rupee or paisa? E.g.: Transaction data

- Amount field is a float field with arbitrary precision(poor JSON conversion). Example: 6611459.8729725825. Typically representation for the money is integer or decimal(float in JSON) with two-digit precision. What do ten digits after decimal represent?

  ![](/images/pulse/float_precision.png)

- In some datasets, "from" and "to" date information is available(transaction) and missing in others(user_device). The only reliable way is to get dates is from the directory and file location.

- Two entries in top transactions for the state Ladakh by pin codes have no name - Pincodes are missing.

  ![](/images/pulse/ladakh_pincode_missing_2019.png)

  ![](/images/pulse/ladakh_pincode_missing_2020.png)

Releasing datasets should be simple keep users(data scientists, analysts) in minds.

Tweet Thread

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">1. <a href="https://twitter.com/PhonePe_?ref_src=twsrc%5Etfw">@PhonePe_</a> recently released Pulse data from their payment data. It was hard to get an overview of the data without doing some data transformation. Here is a thread about data format, transformation, and feedback about data quality. <a href="https://t.co/7QP0RwnL1p">https://t.co/7QP0RwnL1p</a> 🧵</p>&mdash; kracekumar || கிரேஸ்குமார் (@kracetheking) <a href="https://twitter.com/kracetheking/status/1434613848347082756?ref_src=twsrc%5Etfw">September 5, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

## References:

- Pulse Repo: https://github.com/PhonePe/pulse
- Pulse Announcement Tweet: https://twitter.com/PhonePe_/status/1434054060148084736
- Pulse Plus Repo: https://github.com/kracekumar/pulse-plus
- Pulse SQLite DB: https://github.com/kracekumar/pulse-plus/blob/main/data/v1/pulse.db
- Datasette: https://datasette.io/
