---
title: "Dia Duit Dublin, Bye Bengaluru"
date: 2022-08-05T12:13:15+00:00
draft: false
tags: ["Bengaluru", "Dublin", "Stripe", "Life"]
---

**Dia Duit Dublin, Bye Bengaluru**

```
TL;DR: After working for a decade and a year in Bengaluru, I decided to join Stripe in
Dublin, Ireland as a software engineer.

```

![Bangalore Collage](/images/dia-duit-dublin-bye-bengaluru/bangalore_collage.jpg)

When I was in final year of college, I had to choose job location preferences over Chennai and Bengaluru. I choose Bengaluru for two reasons - startups and weather. After working a decade and a year in Bengaluru over seven companies, I decided to leave the startup scene, city, and the country.

I enjoyed my stay in Bengaluru. I spent half a decade building the python community in Bengaluru (of course with immense support from everyone), and felt every second was a complete worth it. Furthermore, I made a lot of acquaintances and some friends. My world view changed every couple of years. I have puzzled at my own beliefs and opinions over time.

I’m happy to call Bengaluru my home and happy being a houseplant. Before leaving the city, I wrote  a [semi-satirical tweets](https://twitter.com/kracetheking/status/1546795418079883264?s=21&t=sVruBcsejUChvX0AVJ5A4g) how to survive in Bangalore. It kind of went viral.

<blockquote class="twitter-tweet" data-dnt="true" data-theme="dark"><p lang="en" dir="ltr">1. 5 tips to survive in <a href="https://twitter.com/hashtag/bangalore?src=hash&amp;ref_src=twsrc%5Etfw">#bangalore</a> for the next decade<br><br>Be a house plant. Cycle/walk for small purchases. Order clothes online. Call up the next door liquor shop for home delivery. Don’t expect Uber or bangalore traffic to get sorted in next 5 to 10 years.</p>&mdash; kracekumar (@kracetheking) <a href="https://twitter.com/kracetheking/status/1546795418079883264?ref_src=twsrc%5Etfw">July 12, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

```
You measure the distance in time on the road
Like life is measured in memories.

You measure the best day by
the quietness of the day and
sipping hot tea
yet looking for your cup of tea.

There are roads and gates you can pass through
But you will never belong there.
There are hands you can never hold
But you can cross the shadow.

There are times when clock moved backwards
And day is dark in the broad daylight,
smokes when nothing was lit
yet you kept moving forwards
you’re tall and short at the same time.
```

<br/>
Now I’m here in Dublin. A small (don’t mistake me, I’m stating based on population) island, 8500 KMS air distance away from Bengaluru. It’s a different kind of summer here and sun sets at 9:15 PM, all day chilly wind in the side of the canal! Never thought so to see sunlight after 7 PM, but heard of the northern lights!

![Dublin Collage](/images/dia-duit-dublin-bye-bengaluru/dublin_collage.jpg)

The main reason to choose Dublin is Stripe. I joined Stripe as a Software Engineer. I have never set my foot in Ireland before, yet I made the choice. After working a decade in startups, Stripe will be the first big company job.

It's been a week in the city and had an interesting encounter. I spotted a calibration problem in one of the weighing machine in the Fresh Food store.
The customers can choose their combinations from a wide range of items.
You fill the salad in the bowl and weigh in the table where you pick up. Once you're in the billing counter, the assistant weighs
the bowl, bills by the weight. I picked up 490 gms of salad and in the counter it weighed 518 gms.
The weight was off by 5%. The store manager acknowledged it but never offered any recourse 🤷‍♂️
[Here is the google map review about the incident](https://goo.gl/maps/GReUHrFVMUeCmMZPA).

It’s scary and anxious to relocate to a country you never visited, stayed, especially during the turbulent economic down turn and during a war between Russia and Ukraine. Everyday layoffs in the air. I’m scared. Anxious. Excited. Happy.

The home search is a nightmare in Dublin. Recently, [r/Dublin](https://www.reddit.com/r/Dublin/comments/vd4tlh/new_subreddit_rrentingindublin/) came up with a separate sub-reddit dedicated to discuss about rental scene in Dublin! I'm told, to find a home, one need to apply to atleast 100 ads to get a single viewing request. I had applied for a fifty ads in [https://daft.ie](daft.ie). The housing market is hot such that the listing that was created last night isn't visible the next day. You setup alerts in the site, try to reply in next 5 minutes else you're out of luck to get a reply or to find it in the site. Fingers crossed.

The folks over here are nice. The city is walkable, clean, and cyclable. The crowd is nothing(tiny compared to Indian street standards) compared to any
big cities in India.

In the uncertainty, only so far the weather app and Google Maps suggestions holds up all the time.

I don’t know a lot of things here. Haven’t even looked up the location of the nearest hospital or pharmacy.

**A song dedicated to Bengaluru**

<iframe width="560" height="315" src="https://www.youtube.com/embed/GLvohMXgcBo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

**Dia Duit Dublin!**
