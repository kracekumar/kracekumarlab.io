---
title: "Render local images in datasette using datasette-render-local-images"
date: 2020-10-17T22:46:38+05:30
draft: false
tags: ["python", "datasette"]
---

[Datasette](https://docs.datasette.io) python library lets publishing a dataset as a SQLite database and
inspects them using a web browser. The database can store text, numbers, arrays,
images, binary data, and all possible formats. datasette-render-images
lets you save the image in the database and render the image using `data-uris`.

Sometimes, while publishing the dataset, you may want to store the images in a
separate directory for various reasons and include the relative path to the
database's images in the table.

The [datasette-render-local-images](https://github.com/kracekumar/datasette-render-local-images/)
plugin lets you perform, rendering the images from the local system.

When the cell value is `images/train/001_inbox.jpg`,
the plugin checks for the path; if the path exists,
the plugin reads the file. Next, checks, whether the file has content,
belongs to one of the image formats, jpg, png, jpeg, and renders the individual cell's image.

Below is the sample image.

![](/images/datasette-render-local-images/sample.png)

The sample image dataset consists of table `books` with
four columns `id, name, author, author_image`.

`author_image` column contains the path to the respective book authors' image.
The first four columns had images satisfying image display criteria.

The next three rows are an example of failure cases.
`No Exit` book author image path doesn't exist; hence plugin displays the exact value.
`Dummy` book author image path exists, but the content is empty; thus, unprocessed the value.
`Scalable_Vector_Graphics` book author image is the format is SVG; therefore, the unprocessed value.

Sometimes the dataset images can be of different sizes, so you may want
to view all the images in the same dimensions. You can modify the image dimension in metadata.json.

The configuration JSON looks like below one.

``` javascript

{
    "plugins": {
        "datasette-render-local-images": {
            "height": 150,
            "width": 150
        }
    }
}

```

The plugins configuration dictionary takes a plugin name as a key and
configuration for the plugin as a dictionary.

In our case, the plugin name is `datasette-render-local-images`, and
the two configurable options height and width.
As of now, the plugin supports only two configurable options.

### Using it in a project

I have a dataset on receipts stored in the table `input`.
Here is a sample out of a row using `sqlite-utils`.

``` bash
$sqlite-utils query ocr.db "select * from input limit 1"

[{"id": "b2df4606e368783ff2d024d0e4e58a620dacfa71",
"filename": "uber_receipts_with_driver_name.png",
"path": "/Users/user/code/work/open_dataset/uber/uber_receipts_with_driver_name.png",
"type": "uber",
"dataset_name": "open_dataset"}]

```

The input table contains five columns, `id, filename, path, type, and dataset_name`.
Let's see the output with and without plugin.

#### Install the plugin

``` bash

$datasette install datasette-render-local-images==0.0.7
...
Successfully installed datasette-render-local-images-0.0.7

```


#### Run the datasette

`$datasette ocr.db`

#### Screenshot of the input table

![](/images/datasette-render-local-images/datasette-image-render-with-no-heigh-width.png)

The images take their height and width and not compact to view.

#### Let's set the default height and width.

Configure height and width in metadata.json

``` javascript


{
    "plugins": {
        "datasette-render-local-images": {
            "height": 100,
            "width": 100
        }
    }
}
```

#### Run datasette command again

`$datasette ocr.db -m metadata.json`

#### Screenshot of the input table

![](/images/datasette-render-local-images/datasette-image-render-local-images-with-metadata.png)

You can play around with different height and width.
Of course, you can `Photo Zoom Plus` or any other extension to zoom the image
while hovering over the image to see the image in full size.

You can report issues, bug, or feature request in the project [github repo](https://github.com/kracekumar/datasette-render-local-images)

### Links from the post:

- Datasette Project Docs - https://docs.datasette.io
- Datasette render local images GitHub - https://github.com/kracekumar/datasette-render-local-images/
