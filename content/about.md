---
title: "About"
date: 2020-06-09T22:38:19+05:30
draft: false
menu: "main"
---

I'm Kracekumar, Software Engineer based out of Dublin. Currently, I work at Stripe building Local Payment Methods in Europe.

I have deep expertise in backend technologies and proven tracken record of tech leadership.
In the past, I have spoken in various technical conferences like PyCon India, Euro Python, PyGotham, etc...

I was the organizer of [Bangalore Python User group](https://www.meetup.com/bangpypers/) and
volunteered for [PyCon India](https://in.pycon.org/) from 2012 to 2016.
In 2017, [I founded RFCs We Love Bangalore Meetup](https://github.com/rfcswelove/rfcs_we_love/commit/aab0b5055132604ac935eae78041890a222ba979)
and [run by various other volunteers](https://www.iiesoc.in/rfcswelove).
The meetup discusses on published RFCs content and discussion around it.

I'm open for new opportunities, to know more about my technical background,
you can read my [CV](/resume/Krace_Resume.pdf).

Outside of tech, I enjoy literature and organize a little meetup, [Dubliners](https://www.meetup.com/meetup-group-hzxiwxve/)
in Dublin.
